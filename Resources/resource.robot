*** Settings ****
Library    BuiltIn

*** Keywords ***

LAUNCH BROWSER
    ${browserSession}=    Open Application  ${APPIUM_URL}  platformName=${PLATFORM_NAME}  platformVersion=${PLATFORM_VERSION}  deviceName=${TEST_DEVICE}  udid=${TEST_DEVICE}  chromedriverPort=${CHROMEPORT}  systemPort=${SYSTEMPORT}  mjpegServerPort=${MJPEGPORT}  usePrebuiltWDA=true  automationName=XCUITest  newCommandTimeout=60000  clearSystemFiles=true  useNewWDA=false  autoDismissAlerts=true 

CLICK ALLOW LOCATION
    click element  ${ALLOW_LOCATION}    