import os

# Device Configuration
TEST_DEVICE= 'AB372CF2-DCD1-4C1F-AF07-310041D239C2'
PLATFORM_NAME = 'iOS'
PLATFORM_VERSION = '14.2'
WDALOCALPORT = '8100'

CHROMEPORT='8300'
SYSTEMPORT='8400'
MJPEGPORT='8500'

# APPIUM_URL = 'http://128.0.0.0:4723/wd/hub'
APPIUM_URL = 'http://localhost:4723/wd/hub'

SITE_URL = 'https://www.5gmark.com/test'

TEST_MY_CONNECTION = 'TEST MY CONNECTION'
WIFI_CONNECTION = '//XCUIElementTypeOther[@name="Wifi"]'
ETHERNET_CONNECTION = '//XCUIElementTypeOther[@name="Ethernet"]'
FIBER_CONNECTION = '//XCUIElementTypeOther[@name="Fiber"]'
OTHER_CONNECTION = '//XCUIElementTypeOther[@name="Other..."]'
RTC_OPTION = '//XCUIElementTypeOther[@name="RTC"]'

# Result page options
RESULT_PAGE = '//XCUIElementTypeStaticText[@name="RESULTS"]'