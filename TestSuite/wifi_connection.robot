*** Settings ***
Library         AppiumLibrary
Resource        ${EXECDIR}/Resources/resource.robot
Variables       ${EXECDIR}/VariableFiles/config.py

*** Test Cases ***

TC-001: Test Wifi Connection
    [Tags]  TestWifiFiberConnection
    LAUNCH BROWSER
    go to url  ${SITE_URL}
    builtin.sleep  10
    click element  ${TEST_MY_CONNECTION}
    wait until page contains element  ${WIFI_CONNECTION}  timeout=10
    click element  ${WIFI_CONNECTION}
    wait until page contains element  ${FIBER_CONNECTION}  timeout=5
    click element  ${FIBER_CONNECTION}
    wait until page contains element  ${RESULT_PAGE}  timeout=120
    capture page screenshot  result.png
    quit application