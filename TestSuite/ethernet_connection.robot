*** Settings ***
Library         AppiumLibrary
Resource        ${EXECDIR}/Resources/resource.robot
Variables       ${EXECDIR}/VariableFiles/config.py

*** Test Cases ***

TC-001: Test Ethernet Connection
    [Tags]  TestEthernetOtherConnection
    LAUNCH BROWSER
    go to url  ${SITE_URL}
    builtin.sleep  10
    click element  ${TEST_MY_CONNECTION}
    wait until page contains element  ${ETHERNET_CONNECTION}  timeout=10
    click element  ${ETHERNET_CONNECTION}
    wait until page contains element  ${OTHER_CONNECTION}  timeout=5
    click element  ${OTHER_CONNECTION}
    wait until page contains element  ${RESULT_PAGE}  timeout=120
    capture page screenshot  result1.png
    quit application

TC-002: Test Ethernet Connection - RTC
    [Tags]  TestEthernetRTCConnection    
    LAUNCH BROWSER
    go to url  ${SITE_URL}
    builtin.sleep  10
    click element  ${TEST_MY_CONNECTION}
    wait until page contains element  ${ETHERNET_CONNECTION}  timeout=10
    click element  ${ETHERNET_CONNECTION}
    wait until page contains element  ${RTC_OPTION}  timeout=5
    click element  ${RTC_OPTION}
    wait until page contains element  ${RESULT_PAGE}  timeout=120
    capture page screenshot  result2.png
    quit application